package com.acme;

/**
 * Description: This class contains main method that returns the number of HTML links on each address provided
 * by command line argument:
 * Ex.:
 * <pre>
 * $ java -jar linkcounter.jar http://acme.com/ http://mysitewithlinks/ http://192.168.2.3/
 * http://acme.com/ 227
 * http://mysitewithlinks/ 23989
 * http://192.168.2.3/ 9456
 * </pre>
 *
 * See README for more information.
 *
 * <p>Copyright 2000-2012, NetSuite, Inc.</p>
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.IOException;
import java.net.MalformedURLException;

public class LinkCounter extends  Thread
{
	private String url;
	private int linkCount;
	
	public LinkCounter(String url){
		this.url = url;
		this.linkCount = 0;
	}
	public String getURL(){
		return url;
	}
	public int getLinkCount(){
		return linkCount;
	}
	public int getCount(String url){
	try{	
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null){ 
            linkCount += countLinks(inputLine);
		}
	}
    catch (MalformedURLException e){
    	e.printStackTrace();
    } 
    catch (IOException e){
    	e.printStackTrace();
    }
    return linkCount;
	}
	public int countLinks(String inputLine){
		int links = 0;
		Pattern pattern = Pattern.compile("<\\s*[aA]\\s*");
		Matcher matcher = pattern.matcher(inputLine);
		while (matcher.find())
			links ++;
		return links;
	}
	@Override
	public void run(){
		getCount(this.url);
		System.out.println(getURL() + "/ " + getLinkCount());
	}
	public static void main(String args[])
	{
		if(args.length < 1){
			System.out.println("[+] - No Arguments supplied nothing to do!");
			System.out.println("\n`...Examle usage  ->java -jar build/libs/linkcounter.jar http://acme.com/ http://mysitewithlinks/ http://192.168.2.3");
			System.exit(1);
		}

		for(int i = 0; i < args.length; i++){
			new LinkCounter(args[i]).start();
		}
	}
}
