=================================================
  Prerequisities
-------------------------------------------------
 - JDK 1.6 installed with JAVA_HOME env variable set to this location 

=================================================
  Instructions
-------------------------------------------------
 - Implement link counter that returns the number of HTML links on each HTTP address provided as a 
   command line argument:
   Ex.:
     $ java -jar build/libs/linkcounter.jar http://acme.com/ http://mysitewithlinks/ http://192.168.2.3/
     http://acme.com/ 66
     http://mysitewithlinks/ 23989
     http://192.168.2.3/ 9456
   
 - Implementation constrains:
   * For simplification assume that the link is defined as '<[whitespace]a[whitespace]' or '<[whitespace]A[whitespace]'. 
     ('<a ', '< a h', '<A >', '<a	attr=' are all valid links)
   * All the processed pages use 8 bit encoding
   * DO NOT use any 3rd party libraries, just what you get from JRE
   * The html pages to process can be very, VERY large!
   * Process the list in parallel.
   * You may write tests to src/test/java using provided libraries (junit, hamcrest, mockito)

=================================================
  Building and Testing
-------------------------------------------------
 - To build the project execute "./gradlew build"
 - To execute tests gradlew build "./gradlew test"

=================================================
  Running the Application
-------------------------------------------------
 - To run the application do the following from the current (this) directory:
   java -jar build/libs/linkcounter.jar http://www.acme.com/ http://mysitewithlinks/ http://192.168.2.3/